//
//  StoryboardID.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/2/1.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import Foundation

struct StoryboardID {
    static let main = "Main"
    static let mainPage = "MainPage"
    static let upload = "NewProductUpload"
    static let shoppingKart = "ShoppingKart"
    static let message = "Message"
    static let setting = "Setting"
    static let card = "Card"
}
