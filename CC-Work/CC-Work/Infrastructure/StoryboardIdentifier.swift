//
//  StoryboardIdentifier.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/2/1.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import Foundation

struct StoryboardIdentifier {
    static let login = "LoginViewController"
    static let main = "MainViewController"
    static let tabBarID = "TabBarController"
    static let upload = "CameraViewController"
    static let shoppingKart = "ShoppingKartViewController"
    static let message = "MessageViewController"
    static let setting = "SettingViewController"
    static let cardPreview = "CardPreviewViewController"
    static let marketPreview = "MarketPreviewViewController"
    static let globalSearch = "GlobalSearchViewController"
}
