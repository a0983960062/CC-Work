//
//  FloatingActionButton.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/2/3.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class FloatingActionButton: UIButtonX {

    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        
        UIView.animate(withDuration: 0.3, animations: {
            if self.transform == .identity {
                self.transform = CGAffineTransform(rotationAngle: 45 * (.pi / 180))
            } else {
                self.transform = .identity
            }
        })
        
        return super.beginTracking(touch, with: event)
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
