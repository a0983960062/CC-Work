//
//  SpecialPlanningDetailViewController.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/3/19.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class SpecialPlanningDetailViewController: UIViewController {
    
    @IBOutlet var specialPlanningImage: UIImageView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var scrollView: UIScrollView!
    
    let photos = [#imageLiteral(resourceName: "img0"), #imageLiteral(resourceName: "img1"), #imageLiteral(resourceName: "img2"), #imageLiteral(resourceName: "img3"), #imageLiteral(resourceName: "img4"), #imageLiteral(resourceName: "img5"), #imageLiteral(resourceName: "img6"), #imageLiteral(resourceName: "img7"), #imageLiteral(resourceName: "img8"), #imageLiteral(resourceName: "img9")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let layout = collectionView.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
        }
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print(scrollView.frame.height)
        scrollView.contentSize.height = specialPlanningImage.frame.height + collectionView.contentSize.height
        
        print(collectionView.frame.size.height)
        print(scrollView.contentSize.height)
        print(collectionView.contentSize.height)
    }

}

extension SpecialPlanningDetailViewController: PinterestLayoutDelegate {
    
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        
        let image = photos[indexPath.item]
        let height = image.size.height
        
        return height
    }
    
}

extension SpecialPlanningDetailViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SpecialPlanningSuggestCollectionViewCell", for: indexPath) as! SpecialPlanningSuggestCollectionViewCell
        
        cell.configCell(image: photos[indexPath.item])
        
        return cell
        
    }
    
}
