//
//  ArtistViewController.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/2/9.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class ArtistViewController: UIViewController {
    
    var photos: [UIImage] = [#imageLiteral(resourceName: "woman-3083463__480.jpg"), #imageLiteral(resourceName: "fashion-3083507__480.jpg"), #imageLiteral(resourceName: "portrait-3087255__480.jpg"), #imageLiteral(resourceName: "sailboat-3113535__480.jpg"), #imageLiteral(resourceName: "moscow-2343483__480.jpg"), #imageLiteral(resourceName: "close-up-1867118__480.jpg")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setRightBarButtonItem()
        // Do any additional setup after loading the view.
    }
    
    func setRightBarButtonItem() {
        
        let searchingImage = UIImage(named: "Search")
        
        let searchingButton = UIBarButtonItem(image: searchingImage, style: .plain, target: self, action: #selector(didTapSearchButton(sender:)))
        
        navigationItem.rightBarButtonItem = searchingButton
        navigationController?.navigationBar.tintColor = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        
    }

}

extension ArtistViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArtistSuggestCollectionViewCell", for: indexPath) as! ArtistSuggestCollectionViewCell
        
        cell.configCell(image: photos[indexPath.item])
        
        return cell
    }
    
}
