//
//  BrandViewController.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/2/9.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class BrandViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setRightBarButtonItem()
        // Do any additional setup after loading the view.
    }
    
    func setRightBarButtonItem() {
        
        let searchingImage = UIImage(named: "Search")
        
        let searchingButton = UIBarButtonItem(image: searchingImage, style: .plain, target: self, action: #selector(didTapSearchButton(sender:)))
        
        navigationItem.rightBarButtonItem = searchingButton
        navigationController?.navigationBar.tintColor = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
