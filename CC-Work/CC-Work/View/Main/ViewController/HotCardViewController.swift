//
//  HotCardViewController.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/2/9.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class HotCardViewController: UIViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    
    var photos: [UIImage] = [#imageLiteral(resourceName: "woman-3083463__480.jpg"), #imageLiteral(resourceName: "fashion-3083507__480.jpg"), #imageLiteral(resourceName: "portrait-3087255__480.jpg"), #imageLiteral(resourceName: "sailboat-3113535__480.jpg"), #imageLiteral(resourceName: "moscow-2343483__480.jpg"), #imageLiteral(resourceName: "close-up-1867118__480.jpg")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "推薦卡片"
        // Do any additional setup after loading the view.
    }

}

extension HotCardViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfColumns: CGFloat = 2
        let width = collectionView.frame.size.width
        let xInsets: CGFloat = 10
        let cellSpacing: CGFloat = 5
        var height: CGFloat!
        
        if Device.IS_IPHONE_5 {
            height = 280
        } else {
            height = 310
        }
        
        return CGSize(width: (width / numberOfColumns) - (xInsets + cellSpacing), height: height)
    }
    
}

extension HotCardViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SuggestCardCollectionViewCell", for: indexPath) as! SuggestCardCollectionViewCell
        
        cell.configCell(image: photos[indexPath.item])
        
        return cell
    }
    
}
