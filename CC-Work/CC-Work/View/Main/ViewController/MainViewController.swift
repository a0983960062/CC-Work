//
//  MainViewController.swift
//  CC-Work
//
//  Created by Charles Chiang on 14/12/2017.
//  Copyright © 2017 Charles Chiang. All rights reserved.
//

import UIKit
import AVFoundation

class MainViewController: UIViewController, UISearchBarDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var scrollView: UIScrollView!
    var selectedIndexPath: IndexPath!
    
    @IBAction func goToSettingPage(_ sender: Any) {
        let storyboard = UIStoryboard(name: StoryboardID.main, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: StoryboardIdentifier.login)
        presentAnimate()
        
        navigationController?.pushViewController(viewController, animated: false)
    }
    
    func configLayout() {
        
        let insetX = (view.frame.width - 320) / 2.0
        
        collectionView.contentInset = UIEdgeInsets(top: 0, left: insetX, bottom: 0, right: insetX)
        
        collectionViewD.contentInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        collectionViewE.contentInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        collectionViewB.contentInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        collectionViewC.contentInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
    }
    
    @IBOutlet var collectionViewB: UICollectionView!
    @IBOutlet var collectionViewC: UICollectionView!
    @IBOutlet var collectionViewD: UICollectionView!
    @IBOutlet var collectionViewE: UICollectionView!
    
    // 企劃
    
    @IBAction func specialPlanning(_ sender: Any) {
        guard let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SpecialPlanningViewController") as? SpecialPlanningViewController else { return }
        
        viewController.navigationItem.title = "特別企劃"
        viewController.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        
        presentAnimate()
        
        show(viewController, sender: self)
    }
    
    // 卡片
    
    @IBAction func hotCard(_ sender: Any) {
        guard let viewController = self.storyboard?.instantiateViewController(withIdentifier: "HotCardViewController") as? HotCardViewController else { return }
        
        viewController.navigationItem.title = "熱門卡片"
        viewController.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        
        presentAnimate()
        
        show(viewController, sender: self)
    }
    
    // 商品
    
    @IBAction func hotProduct(_ sender: Any) {
        guard let viewController = self.storyboard?.instantiateViewController(withIdentifier: "HotProductViewController") as? HotProductViewController else { return }
        
        viewController.navigationItem.title = "熱門商品"
        viewController.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        
        presentAnimate()
        
        show(viewController, sender: self)
    }
    
    // 品牌
    
    @IBAction func brand(_ sender: Any) {
        guard let viewController = self.storyboard?.instantiateViewController(withIdentifier: "BrandViewController") as? BrandViewController else { return }
        
        viewController.navigationItem.title = "品牌館"
        viewController.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        
        presentAnimate()
        
        show(viewController, sender: self)
    }
    
    // 藝術家
    
    @IBAction func artist(_ sender: Any) {
        guard let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ArtistViewController") as? ArtistViewController else { return }
        
        viewController.navigationItem.title = "卡片藝術家"
        viewController.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        
        presentAnimate()
        
        show(viewController, sender: self)
    }
    
    var photos: [UIImage] = [#imageLiteral(resourceName: "woman-3083463__480.jpg"), #imageLiteral(resourceName: "fashion-3083507__480.jpg"), #imageLiteral(resourceName: "portrait-3087255__480.jpg"), #imageLiteral(resourceName: "sailboat-3113535__480.jpg"), #imageLiteral(resourceName: "moscow-2343483__480.jpg"), #imageLiteral(resourceName: "close-up-1867118__480.jpg")]

    var searchbar = UISearchBar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        scrollView.alwaysBounceVertical = true
//        scrollView.isScrollEnabled = true
        setRightBarButtonItem()
        collectionView.delegate = self
        configLayout()
//        scrollView.isScrollEnabled = true
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print(scrollView.frame.size.height)

        print(self.view.frame.size.width)
        print(self.scrollView.frame.size.width)
        self.scrollView.contentSize.height = 1240
        print(scrollView.contentSize.height)
    }
    
    func setRightBarButtonItem() {
        
        let shoppingKartImage = UIImage(named: "ShoppingKart")
        let searchingImage = UIImage(named: "Search")
        
        let shoppingButton = UIButton(type: .custom)
        shoppingButton.setImage(shoppingKartImage, for: UIControlState.normal)
        shoppingButton.addTarget(self, action: #selector(didTapShoppingButton(sender:)), for: UIControlEvents.touchUpInside)
        shoppingButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let shoppingBarButton = UIBarButtonItem(customView: shoppingButton)
        
        let searchingButton = UIButton(type: .custom)
        searchingButton.setImage(searchingImage, for: UIControlState.normal)
        searchingButton.addTarget(self, action: #selector(didTapSearchButton(sender:)), for: UIControlEvents.touchUpInside)
        searchingButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let searchingBarButton = UIBarButtonItem(customView: searchingButton)
        
        navigationItem.rightBarButtonItems = [shoppingBarButton, searchingBarButton]
        navigationController?.navigationBar.tintColor = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collectionView {
            return photos.count
        } else if collectionView == self.collectionViewB {
            return photos.count
        } else if collectionView == self.collectionViewC {
            return photos.count
        } else if collectionView == self.collectionViewD {
            return photos.count
        } else {
            return photos.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionView {
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "MainCollectionViewCell", for: indexPath) as! MainCollectionViewCell
            
            cellA.configCell(image: photos[indexPath.item])
            
            
            return cellA
        } else if collectionView == self.collectionViewB {
            let cellB = collectionView.dequeueReusableCell(withReuseIdentifier: "HotCollectionViewCell", for: indexPath) as! HotCollectionViewCell
            
            cellB.configCell(image: photos[indexPath.item])
            
            return cellB
        } else if collectionView == self.collectionViewC {
            let cellC = collectionView.dequeueReusableCell(withReuseIdentifier: "BrandCollectionViewCell", for: indexPath) as! BrandCollectionViewCell
            
            cellC.configCell(image: photos[indexPath.item])
            
            return cellC
        } else if collectionView == self.collectionViewD {
            let cellD = collectionView.dequeueReusableCell(withReuseIdentifier: "SuggestCollectionViewCell", for: indexPath) as! SuggestCollectionViewCell
            
            cellD.configCell(image: photos[indexPath.item])
            
            return cellD
        } else {
            let cellE = collectionView.dequeueReusableCell(withReuseIdentifier: "ArtistCollectionViewCell", for: indexPath) as! ArtistCollectionViewCell
            
            cellE.configCell(image: photos[indexPath.item])
            
            return cellE
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collectionViewB {
            
            let storyboard = UIStoryboard(name: StoryboardID.card, bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: StoryboardIdentifier.cardPreview) as! CardPreviewViewController
//            presentAnimate()
            viewController.setCardImage(image: photos[indexPath.item])
            
            self.selectedIndexPath = indexPath
            navigationController?.pushViewController(viewController, animated: true)
            
            
        }
    }
    
}

extension UIViewController {
    
    @objc public func didTapShoppingButton(sender: AnyObject) {
        let storyboard = UIStoryboard(name: StoryboardID.shoppingKart, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: StoryboardIdentifier.shoppingKart)
        presentAnimate()
        
        navigationController?.pushViewController(viewController, animated: false)
    }
    
    @objc public func didTapChattingButton(sender: AnyObject) {
        let storyboard = UIStoryboard(name: StoryboardID.message, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: StoryboardIdentifier.message)
        presentAnimate()
        
        navigationController?.pushViewController(viewController, animated: false)
    }
    
    @objc public func didTapSearchButton(sender: AnyObject) {
//        let storyboard = UIStoryboard(name: StoryboardID.mainPage, bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: StoryboardIdentifier.globalSearch)
        
        let searchController = UISearchController(searchResultsController: nil)
        
        searchController.hidesNavigationBarDuringPresentation = false
        
        searchController.searchBar.delegate = self as? UISearchBarDelegate
        searchController.searchBar.placeholder = "搜尋"
        searchController.searchBar.showsCancelButton = true
        
        searchController.searchBar.barTintColor = UIColor.lightGray
        searchController.searchBar.image(for: .search, state: .normal)
        
        present(searchController, animated: true, completion: nil)
        
    }
    
    @objc public func didTapSettingButton() {
        let storyboard = UIStoryboard(name: StoryboardID.setting, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: StoryboardIdentifier.setting)
        presentAnimate()
        
        navigationController?.pushViewController(viewController, animated: false)
    }
    
}

extension MainViewController: UIScrollViewDelegate {

    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if scrollView == self.collectionView {
        
            let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing

            var offset = targetContentOffset.pointee
            let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
            let roundedIndex = round(index)

            offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
            targetContentOffset.pointee = offset
        }
        
    }

}

extension MainViewController: ZoomingViewController {
    func zoomingImageView(for transition: ZoomTransitioningDelegate) -> UIImageView? {
        if let indexPath = selectedIndexPath {
            let cell = collectionViewB.cellForItem(at: indexPath) as! HotCollectionViewCell
            return cell.productImageView
        }
        return nil
    }
    
    func zoomingBackgroundView(for transition: ZoomTransitioningDelegate) -> UIView? {
        return nil
    }
    
}

