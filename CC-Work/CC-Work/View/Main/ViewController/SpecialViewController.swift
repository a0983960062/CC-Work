//
//  SpecialViewController.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/2/7.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class SpecialViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var collectionViewB: UICollectionView!
    
    @IBOutlet var scrollView: UIScrollView!
    
    var photos: [UIImage] = [#imageLiteral(resourceName: "woman-3083463__480.jpg"), #imageLiteral(resourceName: "fashion-3083507__480.jpg"), #imageLiteral(resourceName: "portrait-3087255__480.jpg"), #imageLiteral(resourceName: "sailboat-3113535__480.jpg"), #imageLiteral(resourceName: "moscow-2343483__480.jpg"), #imageLiteral(resourceName: "close-up-1867118__480.jpg")]
    var ad: UIImage = #imageLiteral(resourceName: "Screen Shot 2018-02-08 at 11.26.58 AM.png")
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        if let layout = collectionViewB.collectionViewLayout as? PinterestLayout {
//            layout.delegate = self
//        }
        // Do any additional setup after loading the view.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionView {
            return 1
        } else {
            return photos.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionView {
            
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "SpecialAdCollectionViewCell", for: indexPath) as! SpecialAdCollectionViewCell
            
            cellA.configCell(image: ad)
            
            return cellA
            
        } else {
            
            let cellB = collectionView.dequeueReusableCell(withReuseIdentifier: "SpecialSuggestCollectionViewCell", for: indexPath) as! SpecialSuggestCollectionViewCell
            
            cellB.configCell(image: photos[indexPath.item])
            
            return cellB
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//extension SpecialViewController: PinterestLayoutDelegate {
//
//    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
//
//        if collectionView == self.collectionViewA {
//            return 280
//        } else {
//            return 265.0
//        }
//    }

    
//}

