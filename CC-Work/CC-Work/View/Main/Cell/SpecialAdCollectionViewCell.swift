//
//  SpecialAdCollectionViewCell.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/2/8.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class SpecialAdCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var adImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        cellView.layer.borderWidth = 0.5
//        cellView.layer.borderColor = UIColor.gray.cgColor
        //        cellView.layer.masksToBounds = true
        
//        cellView.layer.cornerRadius = 3
        //        cellView.layer.shadowColor = UIColor.gray.cgColor
        //        cellView.layer.shadowOffset = CGSize(width: 3, height: 3)
        //        cellView.layer.shadowOpacity = 0.3
        //        cellView.layer.shadowRadius = 3
        adImageView.layer.cornerRadius = 3
        adImageView.layer.masksToBounds = true
    }
    
    func configCell(image: UIImage) {
        adImageView.image = image
    }
    
}
