//
//  ArtistSuggestCollectionViewCell.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/3/20.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class ArtistSuggestCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var artistImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        artistImage.layer.cornerRadius = 5
        artistImage.layer.masksToBounds = true
        
        artistImage.layer.shadowColor = UIColor.gray.cgColor
        artistImage.layer.shadowOffset = CGSize(width: 1, height: 2)
        artistImage.layer.shadowOpacity = 0.3
        artistImage.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        
    }
    
    func configCell(image: UIImage) {
        artistImage.image = image
    }
    
}
