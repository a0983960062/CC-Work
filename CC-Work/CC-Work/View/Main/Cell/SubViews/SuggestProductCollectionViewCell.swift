//
//  SuggestProductCollectionViewCell.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/3/21.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class SuggestProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var productImage: UIImageView!
    @IBOutlet var cellView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cellView.layer.borderWidth = 1
        cellView.layer.cornerRadius = 5

        productImage.layer.cornerRadius = 5
        productImage.layer.masksToBounds = true
        
        productImage.layer.shadowColor = UIColor.gray.cgColor
        productImage.layer.shadowOffset = CGSize(width: 1, height: 2)
        productImage.layer.shadowOpacity = 0.3
        productImage.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        
    }
    
    func configCell(image: UIImage) {
        productImage.image = image
    }
    
}
