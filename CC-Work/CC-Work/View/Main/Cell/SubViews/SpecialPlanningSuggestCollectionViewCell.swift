//
//  SpecialPlanningSuggestCollectionViewCell.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/3/19.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class SpecialPlanningSuggestCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var suggestImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        suggestImage.layer.borderWidth = 1
        suggestImage.layer.cornerRadius = 5
        
    }
    
    func configCell(image: UIImage) {
        suggestImage.image = image
    }
    
}
