//
//  BrandCollectionViewCell.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/2/9.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class BrandCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var brandImageView: UIImageView!
    @IBOutlet var cellView: UIView!
    
    @IBOutlet var productA: UIImageView!
    @IBOutlet var productB: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        cellView.layer.borderWidth = 0.5
//        cellView.layer.borderColor = UIColor.gray.cgColor
        //        cellView.layer.masksToBounds = true
        
        cellView.layer.cornerRadius = 5
//        cellView.layer.shadowColor = UIColor.gray.cgColor
//        cellView.layer.shadowOffset = CGSize(width: 3, height: 3)
//        cellView.layer.shadowOpacity = 0.3
//        cellView.layer.shadowRadius = 3
//        cellView.layer.masksToBounds = true
        brandImageView.layer.cornerRadius = 26
        brandImageView.layer.masksToBounds = true
        
        productA.layer.cornerRadius = 3
        productB.layer.cornerRadius = 3
        productA.layer.masksToBounds = true
        productB.layer.masksToBounds = true
        
    }
    
    override func layoutSubviews() {
        self.contentView.layer.cornerRadius = 5
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 5
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
    }
    
    func configCell(image: UIImage) {
        brandImageView.image = image
    }
    
}
