//
//  MainCollectionViewCell.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/1/29.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class MainCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var productImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
        productImageView.layer.cornerRadius = 5
        productImageView.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        self.contentView.layer.cornerRadius = 5
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 5
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
    }
    
    func configCell(image: UIImage) {
        productImageView.image = image
    }
    
}
