//
//  RegisterViewController.swift
//  CC-Work
//
//  Created by Charles Chiang on 04/01/2018.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit
import AVFoundation

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var memberID: UITextField!
    @IBOutlet var memberPassword: UITextField!
    @IBOutlet var retypePassword: UITextField!
    @IBOutlet var memberName: UITextField!
    @IBOutlet var birthday: UITextField!
    @IBOutlet var phoneNumber: UITextField!
    @IBOutlet var email: UITextField!
    
    @IBOutlet var checkBoxOutlet: UIButton!
    @IBOutlet var buttonOutlet: UIButton!
    
    var isChecked: Bool = false
    
    @IBAction func checkBox(_ sender: CheckBox) {
        if isChecked == false {
            self.checkBoxOutlet.setImage(UIImage(named: "memberAllow-Agree"), for: UIControlState.normal)
            self.buttonOutlet.backgroundColor = UIColor(red: 76.0/255.0, green: 185.0/255.0, blue: 6.0/255.0, alpha: 1.0)
            self.buttonOutlet.isEnabled = true
            isChecked = true
        } else if isChecked == true {
            self.checkBoxOutlet.setImage(UIImage(named: "memberAllow"), for: UIControlState.normal)
            self.buttonOutlet.backgroundColor = UIColor(red: 149.0/255.0, green: 152.0/255.0, blue: 154.0/255.0, alpha: 1.0)
            self.buttonOutlet.isEnabled = false
            isChecked = false
        }
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        if memberPassword.text != retypePassword.text {
            self.buttonOutlet.isEnabled = false
            self.retypePassword.layer.borderColor = UIColor.red.cgColor
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldLayout()
        setupDatePicker()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.handleTap))
        self.scrollView.addGestureRecognizer(tap)
        setupViewResizerOnKeyboardShown()
        self.buttonOutlet.isEnabled = false
        // Do any additional setup after loading the view.
    }
    
    func textFieldLayout() {
        
        let borderColor = UIColor(red: 187.0/255.0, green: 187.0/255.0, blue: 187.0/255.0, alpha: 1.0)
        
        self.memberID.layer.borderWidth = 1.0
        self.memberID.layer.cornerRadius = 5.0
        self.memberID.layer.borderColor = borderColor.cgColor
        self.memberPassword.layer.borderWidth = 1.0
        self.memberPassword.layer.cornerRadius = 5.0
        self.memberPassword.layer.borderColor = borderColor.cgColor
        self.retypePassword.layer.borderWidth = 1.0
        self.retypePassword.layer.cornerRadius = 5.0
        self.retypePassword.layer.borderColor = borderColor.cgColor
        self.memberName.layer.borderWidth = 1.0
        self.memberName.layer.cornerRadius = 5.0
        self.memberName.layer.borderColor = borderColor.cgColor
        self.birthday.layer.borderWidth = 1.0
        self.birthday.layer.cornerRadius = 5.0
        self.birthday.layer.borderColor = borderColor.cgColor
        self.phoneNumber.layer.borderWidth = 1.0
        self.phoneNumber.layer.cornerRadius = 5.0
        self.phoneNumber.layer.borderColor = borderColor.cgColor
        self.email.layer.borderWidth = 1.0
        self.email.layer.cornerRadius = 5.0
        self.email.layer.borderColor = borderColor.cgColor
    }
    
    func setupDatePicker() {
        
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = UIDatePickerMode.date
        datePicker.addTarget(self, action: #selector(RegisterViewController.datePickerValueChanged(sender:)), for: UIControlEvents.valueChanged)
        birthday.inputView = datePicker
        
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.medium
        formatter.timeStyle = DateFormatter.Style.none
        birthday.text = formatter.string(from: sender.date)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIViewController {
    
    @objc func handleTap() {
        view.endEditing(true)
    }
    
    func setupViewResizerOnKeyboardShown() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(UIViewController.keyboardWillShowForResizing),
                                               name: Notification.Name.UIKeyboardWillShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(UIViewController.keyboardWillHideForResizing),
                                               name: Notification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    @objc func keyboardWillShowForResizing(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            let window = self.view.window?.frame {
            // We're not just minusing the kb height from the view height because
            // the view could already have been resized for the keyboard before
            self.view.frame = CGRect(x: self.view.frame.origin.x,
                                     y: self.view.frame.origin.y,
                                     width: self.view.frame.width,
                                     height: window.origin.y + window.height - keyboardSize.height)
        } else {
            debugPrint("We're showing the keyboard and either the keyboard size or window is nil: panic widely.")
        }
    }
    
    @objc func keyboardWillHideForResizing(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let viewHeight = self.view.frame.height
            self.view.frame = CGRect(x: self.view.frame.origin.x,
                                     y: self.view.frame.origin.y,
                                     width: self.view.frame.width,
                                     height: viewHeight + keyboardSize.height)
        } else {
            debugPrint("We're about to hide the keyboard and the keyboard size is nil. Now is the rapture.")
        }
    }
    
}

