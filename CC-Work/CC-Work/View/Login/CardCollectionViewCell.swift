//
//  CardCollectionViewCell.swift
//  CC-Work
//
//  Created by Charles Chiang on 15/12/2017.
//  Copyright © 2017 Charles Chiang. All rights reserved.
//

import UIKit

class CardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var image: UIImageView!
    
    func configCell(image: UIImage) {
        self.image.image = image
    }
    
}
