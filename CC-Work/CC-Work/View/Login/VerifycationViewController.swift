//
//  VerifycationViewController.swift
//  CC-Work
//
//  Created by Charles Chiang on 12/01/2018.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class VerifycationViewController: UIViewController {
    
    @IBAction func segmentedControl(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            emailView.isHidden = true
            phoneView.isHidden = false
        } else if sender.selectedSegmentIndex == 1 {
            emailView.isHidden = false
            phoneView.isHidden = true
        }
    }
    
    @IBOutlet var phoneView: UIView!
    @IBOutlet var verifycationCode: UITextField!
    @IBOutlet var resendCodeButton: UIButton!
    @IBOutlet var phoneConfirmButtonOutlet: UIButton!
    @IBOutlet var phoneDownCount: UILabel!
    
    @IBOutlet var emailView: UIView!
    @IBOutlet var emailConfirmResendButtonOutlet: UIButton!
    @IBOutlet var emailDownCount: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setPhoneViewOutlet()
        setEmailOutlet()
        emailView.isHidden = true
        
        resendCodeButton.isEnabled = false
        phoneConfirmButtonOutlet.isEnabled = false
        emailConfirmResendButtonOutlet.isEnabled = false
        
        verifycationCode.addTarget(self, action: #selector(verifycationCodeTextFieldDidChange(_:)), for: .editingChanged)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.handleTap))
        self.phoneView.addGestureRecognizer(tap)
        self.emailView.addGestureRecognizer(tap)
        setupViewResizerOnKeyboardShown()
        
        startTimer()
        // Do any additional setup after loading the view.
    }
    
    
    var countdownTimer: Timer!
    var totalTime = 50
    
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        phoneDownCount.text = "\(timeFormatted(totalTime))秒後可重新發送"
        emailDownCount.text = "驗證信已傳送至您的信箱，請至信箱點擊連結，或再 \(timeFormatted(totalTime)) 秒後重新發送驗證信。"
        
        if totalTime != 0 {
            totalTime -= 1
        } else {
            endTimer()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.resignFirstResponder()
    }
    
    func endTimer() {
        resendCodeButton.isEnabled = true
        resendCodeButton.backgroundColor = UIColor(red: 76.0/255.0, green: 185.0/255.0, blue: 6.0/255.0, alpha: 1.0)
        emailConfirmResendButtonOutlet.isEnabled = true
        emailConfirmResendButtonOutlet.backgroundColor = UIColor(red: 76.0/255.0, green: 185.0/255.0, blue: 6.0/255.0, alpha: 1.0)
        emailDownCount.text = "驗證信已傳送至您的信箱，請至信箱點擊連結，或再重新發送驗證信。"
        countdownTimer.invalidate()
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
//        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%2d", seconds)
    }
    
    func setPhoneViewOutlet() {
        
        verifycationCode.layer.borderWidth = 1.0
        verifycationCode.layer.cornerRadius = 5.0
        verifycationCode.layer.borderColor = UIColor(red: 187.0/255.0, green: 187.0/255.0, blue: 187.0/255.0, alpha: 1.0).cgColor
        
        resendCodeButton.layer.cornerRadius = 5.0
        phoneConfirmButtonOutlet.layer.cornerRadius = 5.0
        
    }
    
    func setEmailOutlet() {
        emailConfirmResendButtonOutlet.layer.cornerRadius = 5.0
    }
    
    @objc func verifycationCodeTextFieldDidChange(_ textField: UITextField) {
        if self.verifycationCode.text == "" {
            phoneConfirmButtonOutlet.isEnabled = false
            phoneConfirmButtonOutlet.backgroundColor = UIColor.lightGray
        } else if self.verifycationCode.text != "123" {
            phoneConfirmButtonOutlet.isEnabled = true
            phoneConfirmButtonOutlet.backgroundColor = UIColor(red: 76.0/255.0, green: 185.0/255.0, blue: 6.0/255.0, alpha: 1.0)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
