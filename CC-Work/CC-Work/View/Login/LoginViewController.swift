//
//  LoginViewController.swift
//  CC-Work
//
//  Created by Charles Chiang on 12/12/2017.
//  Copyright © 2017 Charles Chiang. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import GoogleSignIn
import Google
import FBSDKLoginKit

class LoginViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate, FBSDKLoginButtonDelegate {
    
    @IBOutlet var subView: UIView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var originalLoginButton: UIButton!
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            print(error)
            return
        }
        self.getFacebookLoginUserInfo(completion: { userInfo, error in
            if let error = error { print(error.localizedDescription)}
        })
    }
        
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("Logged out")
    }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if error != nil{
            print(error ?? "google error")
            return
        }
        print(user.profile.email)
        let url: URL = user.profile.imageURL(withDimension: 154)
        getProfilePicture(url: url)
        print(url)

    }
    
    func getProfilePicture(url: URL) {
        let session = URLSession(configuration: .default)
        
        let getImageFromUrl = session.dataTask(with: url) { (data, response, error) in
            
            if let e = error {
                print("Error Occurred: \(e)")
            } else {
                if (response as? HTTPURLResponse) != nil {
                    if let imageData = data {
                        let image = UIImage(data: imageData)
                        self.profileImage.image = image
                    } else {
                        print("Image file is currupted")
                    }
                } else {
                    print("No response from server")
                }
            }
        }
    }
    
    var dict : [String : AnyObject]!
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    
    func facebookLogin() {
        
        let facebookLoginButton = FBSDKLoginButton()
        view.addSubview(facebookLoginButton)
        if Device.IS_IPHONE_X {
            facebookLoginButton.frame = CGRect(x: 188, y: 540, width: 122, height: 40)
        } else if Device.IS_IPHONE_6 {
            facebookLoginButton.frame = CGRect(x: 188, y: 530, width: 122, height: 40)
        } else if Device.IS_IPHONE_5 {
            facebookLoginButton.frame = CGRect(x: 160.5, y: 455, width: 120, height: 40)
        } else if Device.IS_IPHONE_6P {
            facebookLoginButton.frame = CGRect(x: 210, y: 530, width: 122, height: 40)
        }
    
        facebookLoginButton.delegate = self
        facebookLoginButton.layer.shadowColor = UIColor.gray.cgColor
        facebookLoginButton.layer.shadowOpacity = 1.0
        facebookLoginButton.layer.shadowOffset = CGSize(width: 0, height: 2)
        facebookLoginButton.readPermissions = ["email", "public_profile"]
        
//        let loginManager = LoginManager()
//
//        loginManager.logIn(readPermissions: [.publicProfile, .email], viewController: self) { result in
//
//            switch result {
//            case .failed(let error):
//                print(error.localizedDescription)
//            case .cancelled:
//                print("User cancelled the login")
//            case .success(_,_,_):
//                self.getFacebookLoginUserInfo{ userInfo, error in
//                    if let error = error { print(error.localizedDescription)}
//                }
//            }
//        }
    }
    
    func googleLogin() {
        
        var error: NSError?
        GGLContext.sharedInstance().configureWithError(&error)
        
        if error != nil {
            print(error)
            return
        }
        
        let googleSignInButton = GIDSignInButton()
        view.addSubview(googleSignInButton)
        
//        let x: Int = Int((view.frame.width / 2) - (googleSignInButton.frame.width) + 20)
        if Device.IS_IPHONE_X {
            googleSignInButton.frame = CGRect(x: 59, y: 535, width: 122, height: 30)
        } else if Device.IS_IPHONE_6 {
            googleSignInButton.frame = CGRect(x: 59, y: 525, width: 122, height: 30)
        } else if Device.IS_IPHONE_5 {
            googleSignInButton.frame = CGRect(x: 34.5, y: 450, width: 120, height: 30)
        } else if Device.IS_IPHONE_6P {
            googleSignInButton.frame = CGRect(x: 79, y: 525, width: 122, height: 30)
        }
        
        googleSignInButton.colorScheme = .light
        googleSignInButton.layer.shadowColor = UIColor.gray.cgColor
        googleSignInButton.layer.shadowOpacity = 0.3
        googleSignInButton.layer.shadowOffset = CGSize(width: 0, height: 2)
        
        googleSignInButton.style = .standard
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
    }
    
    func getFacebookLoginUserInfo(completion: @escaping (_ : [String: Any]?, _ : Error?) -> Void) {
        
        let request = GraphRequest(graphPath: "me", parameters: ["fields" : "id, name, email, picture"])
        
        request.start { response, result in
            switch result {
            case .failed(let error):
                completion(nil, error)
            case .success(let graphResponse):
                completion(graphResponse.dictionaryValue, nil)
                print(graphResponse.dictionaryValue!)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        profileImage.layer.masksToBounds = false
        setupViewResizerOnKeyboardShown()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.handleTap))
        self.scrollView.addGestureRecognizer(tap)
        userName.layer.borderColor = UIColor.gray.cgColor
        
        self.facebookLogin()
        self.googleLogin()
        self.userName.placeholder = "帳號"
        self.password.placeholder = "密碼"        // Do any additional setup after loading the view.
        
        setConstraint()
        scrollView.isScrollEnabled = false
    }
    
    func setConstraint() {
        let otherUpConstraint = NSLayoutConstraint(item: profileImage, attribute: .top, relatedBy: .equal, toItem: super.view, attribute: .top, multiplier: 1, constant: 139)
        let otherDownConstraint = NSLayoutConstraint(item: subView, attribute: .top, relatedBy: .equal, toItem: profileImage, attribute: .bottom, multiplier: 1, constant: 60)
        let SEUpConstraint = NSLayoutConstraint(item: profileImage, attribute: .top, relatedBy: .equal, toItem: super.view, attribute: .top, multiplier: 1, constant: 80)
        let SEDownConstraint = NSLayoutConstraint(item: subView, attribute: .top, relatedBy: .equal, toItem: profileImage, attribute: .bottom, multiplier: 1, constant: 40)
        if Device.IS_IPHONE_5 {
            view.addConstraints([SEUpConstraint, SEDownConstraint])
        } else {
            view.addConstraints([otherUpConstraint, otherDownConstraint])
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        userName.resignFirstResponder()
        password.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

struct Device {
    // iDevice detection code
    static let IS_IPAD             = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPHONE           = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_RETINA           = UIScreen.main.scale >= 2.0
    
    static let SCREEN_WIDTH        = Int(UIScreen.main.bounds.size.width)
    static let SCREEN_HEIGHT       = Int(UIScreen.main.bounds.size.height)
    static let SCREEN_MAX_LENGTH   = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let SCREEN_MIN_LENGTH   = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )
    
    static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH  < 568
    static let IS_IPHONE_5         = IS_IPHONE && SCREEN_MAX_LENGTH == 568
    static let IS_IPHONE_6         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_6P        = IS_IPHONE && SCREEN_MAX_LENGTH == 736
    static let IS_IPHONE_X         = IS_IPHONE && SCREEN_MAX_LENGTH == 812
}

