//
//  WelcomeViewController.swift
//  CC-Work
//
//  Created by Charles Chiang on 17/01/2018.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet var confirmButton: UIButton!
    
    @IBAction func confirmButtonPressed(_ sender: Any) {
        
        
        let storyBoard = UIStoryboard(name: StoryboardID.mainPage, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: StoryboardIdentifier.tabBarID)
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
    let images: [UIImage] = [UIImage(named: "01")!, UIImage(named: "02")!, UIImage(named: "03")!, UIImage(named: "04")!, UIImage(named: "05")!, UIImage(named: "06")!, UIImage(named: "07")!, UIImage(named: "08")!, UIImage(named: "09")!]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.confirmButton.layer.cornerRadius = 5
        // Do any additional setup after loading the view.
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GiftCollectionViewCell", for: indexPath) as! GiftCollectionViewCell
        
        cell.configCell(image: images[indexPath.row])
        cell.whiteMaskView.isHidden = true
        
        
        collectionView.allowsMultipleSelection = true
        
//        if Device.IS_IPHONE_5 {
//            cell.sizeThatFits(CGSize(width: 87.0, height: 156.0))
//        } else {
//            cell.sizeThatFits(CGSize(width: 105.0, height: 186.0))
//        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GiftCollectionViewCell", for: indexPath) as! GiftCollectionViewCell
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
