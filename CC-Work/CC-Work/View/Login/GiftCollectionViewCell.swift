//
//  GiftCollectionViewCell.swift
//  CC-Work
//
//  Created by Charles Chiang on 17/01/2018.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class GiftCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var giftImage: UIImageView!
    @IBOutlet var whiteMaskView: UIView!
    
    func configCell(image: UIImage) {
        self.giftImage.image = image
    }
    
    override var isSelected: Bool {
        didSet{
            if self.isSelected {
                self.whiteMaskView.isHidden = false
            } else {
                self.whiteMaskView.isHidden = true
            }
        }
    }

}
