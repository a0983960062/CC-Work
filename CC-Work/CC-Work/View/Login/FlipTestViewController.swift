//
//  FlipTestViewController.swift
//  CC-Work
//
//  Created by Charles Chiang on 14/12/2017.
//  Copyright © 2017 Charles Chiang. All rights reserved.
//

import UIKit

class FlipTestViewController: UIViewController {

    @IBOutlet var flipImage: UIButton!
    var isOpen = false
    
    @IBAction func flipButton(_ sender: UIButton) {
        
        if isOpen {
            isOpen = false
            let image = UIImage(named: "front")
            flipImage.setImage(image, for: .normal)
            UIView.transition(with: flipImage, duration: 0.75, options: .transitionFlipFromLeft, animations: nil, completion: nil)
        } else {
            isOpen = true
            let image = UIImage(named: "back")
            flipImage.setImage(image, for: .normal)
            UIView.transition(with: flipImage, duration: 0.75, options: .transitionFlipFromRight, animations: nil, completion: nil)
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
