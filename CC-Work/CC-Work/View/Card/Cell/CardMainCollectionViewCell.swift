//
//  CardMainCollectionViewCell.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/2/12.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class CardMainCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var cardImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cardImageView.layer.cornerRadius = 5
        cardImageView.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        self.contentView.layer.cornerRadius = 5
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 5
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
    }
    
    func configCell(image: UIImage) {
        cardImageView.image = image
    }
    
}
