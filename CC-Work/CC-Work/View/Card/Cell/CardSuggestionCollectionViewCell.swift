//
//  CardSuggestionCollectionViewCell.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/3/12.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class CardSuggestionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var cardImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cardImage.layer.cornerRadius = 5
        cardImage.layer.masksToBounds = true
        
        cardImage.layer.shadowColor = UIColor.gray.cgColor
        cardImage.layer.shadowOffset = CGSize(width: 1, height: 2)
        cardImage.layer.shadowOpacity = 0.3
        cardImage.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        
    }
    
    func configCell(image: UIImage) {
        cardImage.image = image
    }
    
}
