//
//  MarketSuggestionCollectionViewCell.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/3/13.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class MarketSuggestionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var cardImage: UIImageView!
    @IBOutlet var cellView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cardImage.layer.cornerRadius = 5
        cardImage.layer.masksToBounds = true
        
        cellView.layer.cornerRadius = 5
        
    }
    
    override func layoutSubviews() {
        self.contentView.layer.cornerRadius = 5
        self.contentView.layer.masksToBounds = true
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 5
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
    }
    
    func configCell(image: UIImage) {
        cardImage.image = image
    }
    
}
