//
//  MarketMainCollectionViewCell.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/2/13.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class MarketMainCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var marketImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        marketImageView.layer.cornerRadius = 5
        marketImageView.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        self.contentView.layer.cornerRadius = 5
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 5
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
    }
    
    func configCell(image: UIImage) {
        marketImageView.image = image
    }
    
}
