//
//  MarketMainViewController.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/2/13.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit
import Messages

class MarketMainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet var mainCollectionView: UICollectionView!
    @IBOutlet var suggestCollectionView: UICollectionView!
    @IBOutlet var scrollView: UIScrollView!
    
    var selectedCollection: UICollectionView!
    var selectedIndexPath: IndexPath!
    
    var photos: [UIImage] = [#imageLiteral(resourceName: "close-up-1867118__480.jpg"), #imageLiteral(resourceName: "moscow-2343483__480.jpg"), #imageLiteral(resourceName: "sailboat-3113535__480.jpg"), #imageLiteral(resourceName: "portrait-3087255__480.jpg"), #imageLiteral(resourceName: "fashion-3083507__480.jpg"), #imageLiteral(resourceName: "woman-3083463__480.jpg") ]

    override func viewDidLoad() {
        super.viewDidLoad()

        scrollView.alwaysBounceVertical = true
        setRightBarButtonItem()
        setLeftBarButtonItem()
        setTitle()
        configCollectionView()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        print(self.scrollView.frame.size.height)
        
        scrollView.contentSize.height = mainCollectionView.contentSize.height + suggestCollectionView.contentSize.height + 65
    
        print(self.scrollView.contentSize.height)
        
        print(suggestCollectionView.frame.size.height)
        print(suggestCollectionView.contentSize.height)
        
    }
    
    func configCollectionView() {
        
        let insetX = (view.frame.width - 320) / 2.0
        
        mainCollectionView.contentInset = UIEdgeInsets(top: 0, left: insetX, bottom: 0, right: insetX)
        suggestCollectionView.contentInset = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
        
       
        
    }
    
    func setTitle() {
        self.navigationItem.title = "商品列表"
    }
    
    func setLeftBarButtonItem() {
        
        let unicornImage = UIImage(named: "Image3")
        
        let settingButton = UIButton(type: .custom)
        settingButton.setImage(unicornImage, for: UIControlState.normal)
        settingButton.addTarget(self, action: #selector(didTapSettingButton), for: UIControlEvents.touchUpInside)
        settingButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let settingBarButton = UIBarButtonItem(customView: settingButton)
        
        navigationItem.leftBarButtonItem = settingBarButton
        
    }
    
    func setRightBarButtonItem() {
        
        let shoppingKartImage = UIImage(named: "ShoppingKart")
        let searchImage = UIImage(named: "Search")
        
        let shoppingButton = UIButton(type: .custom)
        shoppingButton.setImage(shoppingKartImage, for: UIControlState.normal)
        shoppingButton.addTarget(self, action: #selector(didTapShoppingButton(sender:)), for: UIControlEvents.touchUpInside)
        shoppingButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let shoppingBarButton = UIBarButtonItem(customView: shoppingButton)
        
        let searchingButton = UIButton(type: .custom) 
        searchingButton.setImage(searchImage, for: UIControlState.normal)
        searchingButton.addTarget(self, action: #selector(didTapSearchButton(sender:)), for: UIControlEvents.touchUpInside)
        searchingButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let searchingBarButton = UIBarButtonItem(customView: searchingButton)
        
        navigationItem.rightBarButtonItems = [shoppingBarButton, searchingBarButton]
        navigationController?.navigationBar.tintColor = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == mainCollectionView {
            return photos.count
        } else {
            return photos.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == mainCollectionView{
            
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "MarketMainCollectionViewCell", for: indexPath) as! MarketMainCollectionViewCell
            
            cellA.configCell(image: photos[indexPath.item])
            
            return cellA
            
        } else {
            
            let cellB = collectionView.dequeueReusableCell(withReuseIdentifier: "MarketSuggestionCollectionViewCell", for: indexPath) as! MarketSuggestionCollectionViewCell
            
            cellB.configCell(image: photos[indexPath.item])
            
            return cellB
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
        let viewController = storyboard?.instantiateViewController(withIdentifier: StoryboardIdentifier.marketPreview) as! MarketPreviewViewController
            
        viewController.setMarketImage(image: photos[indexPath.item])
            
            //        presentAnimate()
            
        self.selectedCollection = collectionView
        self.selectedIndexPath = indexPath
        
        navigationController?.pushViewController(viewController, animated: true)
    }

}

extension MarketMainViewController: ZoomingViewController {
    func zoomingImageView(for transition: ZoomTransitioningDelegate) -> UIImageView? {
        
        if selectedCollection == mainCollectionView {
            
            if let indexPath = selectedIndexPath {
                let cell = mainCollectionView.cellForItem(at: indexPath) as!MarketMainCollectionViewCell
                return cell.marketImageView
            }
            return nil
            
        } else {
            
            if let indexPath = selectedIndexPath {
                let cell = suggestCollectionView.cellForItem(at: indexPath) as!MarketSuggestionCollectionViewCell
                return cell.cardImage
            }
            return nil
            
        }
        
    }
    
    func zoomingBackgroundView(for transition: ZoomTransitioningDelegate) -> UIView? {
        return nil
    }
    
    
}

extension MarketMainViewController: UIScrollViewDelegate {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if scrollView == mainCollectionView {
            
            let layout = self.mainCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
            let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
            
            var offset = targetContentOffset.pointee
            let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
            let roundedIndex = round(index)
            
            offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
            targetContentOffset.pointee = offset
            
        }
        
    }
    
}
