//
//  PersonalPageViewController.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/2/5.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class PersonalPageViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    let languages = ["English", "Chinese (Tranditional)", "Chinese (Simplified)", "Japanese", "Thai"]
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return languages.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return languages[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch languages[row] {
        case "English":
            changeLanguageENG()
            print("English")
        case "Chinese (Tranditional)":
            changeLanguageCHT()
            print("Chinese (Tranditional)")
        case "Chinese (Simplified)":
            changeLanguageCHS()
            print("Chinese (Simplified)")
        case "Japanese":
            changeLanguageJPA()
            print("Japanese")
        case "Thai":
            changeLanguageTHA()
            print("Thai")
        default:
            break
        }
    }
    
    
    @IBOutlet var label: UILabel!
    
    @IBOutlet var pickerView: UIPickerView!
    
    
    @IBAction func goToNewProductUploadPage(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: StoryboardID.upload, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: StoryboardIdentifier.upload)
        presentAnimate()
        
        present(viewController, animated: false, completion: nil)
    }
    
    @IBAction func gotoGiftPage(_ sender: Any) {
        
        guard let viewController = self.storyboard?.instantiateViewController(withIdentifier: "GiftViewController") as? GiftViewController else { return }
        
        viewController.navigationItem.title = "轉贈朋友"
        viewController.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        
        presentAnimate()
        
        show(viewController, sender: self)
        
    }
    
    @IBAction func gotoSellPage(_ sender: Any) {
        
        guard let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SellViewController") as? SellViewController else { return }
        
        viewController.navigationItem.title = "標價出售"
        viewController.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        
        presentAnimate()
        
        show(viewController, sender: self)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(self.changeLanguage), name: NSNotification.Name(rawValue: "LanguageChanged"), object: nil)
        
        label.text = LanguageHelper.getString(key: "Test")
        // Do any additional setup after loading the view.
    }
    
    
    @objc func changeLanguage() -> Void {
        label?.text = LanguageHelper.getString(key: "Test")
        
    }
    func changeLanguageENG() -> Void {
        LanguageHelper.shareInstance.setLanguage(langeuage: "en")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LanguageChanged"), object: nil)
    }
    func changeLanguageCHT() -> Void {
        LanguageHelper.shareInstance.setLanguage(langeuage: "zh-Hant")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LanguageChanged"), object: nil)
    }
    
    func changeLanguageCHS() -> Void {
        LanguageHelper.shareInstance.setLanguage(langeuage: "zh-Hans")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LanguageChanged"), object: nil)
    }
    
    func changeLanguageJPA() -> Void {
        LanguageHelper.shareInstance.setLanguage(langeuage: "ja")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LanguageChanged"), object: nil)
    }
    
    func changeLanguageTHA() -> Void {
        LanguageHelper.shareInstance.setLanguage(langeuage: "th")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LanguageChanged"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
