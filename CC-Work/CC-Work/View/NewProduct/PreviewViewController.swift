//
//  PreviewViewController.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/2/2.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class PreviewViewController: UIViewController {

    @IBOutlet var photo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        photo.image = image
        // Do any additional setup after loading the view.
    }
    
    var image: UIImage!
    
    @IBAction func cancelButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveButton(_ sender: Any) {
        
//        let size = CGSize(width: (photo.image?.size.width)!, height: (photo.image?.size.height)!)
//        UIGraphicsBeginImageContext(size)
//
//        let areaSize = CGRect(x: 0, y: 0, width: size.width, height: size.height)
//        photo.image?.draw(in: areaSize)
        
        UIImageWriteToSavedPhotosAlbum(photo.image!, nil, nil, nil)
        dismiss(animated: true, completion: nil)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
