//
//  FriendsViewController.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/2/3.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class FriendsViewController: UIViewController {

    @IBOutlet var floatingActionButton: UIButton!
    @IBOutlet var menuView: UIViewX!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        floatingActionButton.layer.cornerRadius = 25
        closeMenu()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func menuTapped(_ sender: FloatingActionButton) {
        
        UIView.animate(withDuration: 0.3, animations: {
            if self.menuView.transform == .identity {
                self.closeMenu()
            } else {
                self.menuView.transform = .identity
            }
        })
        
    }
    
    func closeMenu() {
        menuView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
