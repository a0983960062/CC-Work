//
//  MarketPreviewViewController.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/2/13.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class MarketPreviewViewController: UIViewController {
    
    @IBOutlet var previewImage: UIImageView!
    @IBOutlet var brandName: UIButton!
    
    var image: UIImage?
    
    @IBAction func brandName(_ sender: Any) {
        
        guard let viewController = self.storyboard?.instantiateViewController(withIdentifier: "BrandDetailViewController") as? BrandDetailViewController else { return }
        
        presentAnimate()
        
        let title = brandName.currentTitle!
        viewController.navigationItem.title = title
        show(viewController, sender: self)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setRightBarButtonItem()
        self.previewImage.image = image!
        // Do any additional setup after loading the view.
    }
    
    func setRightBarButtonItem() {
        
        let shoppingKartImage = UIImage(named: "ShoppingKart")
        let chattingImage = UIImage(named: "message")
        let searchImage = UIImage(named: "Search")
        
        let shoppingButton = UIButton(type: .custom)
        shoppingButton.setImage(shoppingKartImage, for: UIControlState.normal)
        shoppingButton.addTarget(self, action: #selector(didTapShoppingButton(sender:)), for: UIControlEvents.touchUpInside)
        shoppingButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let shoppingBarButton = UIBarButtonItem(customView: shoppingButton)
        
        let chattingButton = UIButton(type: .custom)
        chattingButton.setImage(chattingImage, for: UIControlState.normal)
        chattingButton.addTarget(self, action: #selector(didTapChattingButton(sender:)), for: UIControlEvents.touchUpInside)
        chattingButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let chattingBarButton = UIBarButtonItem(customView: chattingButton)
        
        let searchingButton = UIButton(type: .custom)
        searchingButton.setImage(searchImage, for: UIControlState.normal)
        searchingButton.addTarget(self, action: #selector(didTapSearchButton(sender:)), for: UIControlEvents.touchUpInside)
        searchingButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let searchingBarButton = UIBarButtonItem(customView: searchingButton)
        
        navigationItem.rightBarButtonItems = [chattingBarButton, shoppingBarButton, searchingBarButton]
        navigationController?.navigationBar.tintColor = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        
    }
    
    func setMarketImage(image: UIImage) {
        self.image = image
    }

}

extension MarketPreviewViewController: ZoomingViewController {
    
    func zoomingImageView(for transition: ZoomTransitioningDelegate) -> UIImageView? {
        return previewImage
    }
    
    func zoomingBackgroundView(for transition: ZoomTransitioningDelegate) -> UIView? {
        return nil
    }
    
}
