//
//  CardPreviewViewController.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/2/12.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class CardPreviewViewController: UIViewController {
    
    @IBOutlet var preViewImage: UIImageView!
    @IBOutlet var artistName: UIButton!
    
    var image: UIImage?
    
    @IBAction func artistName(_ sender: Any) {
        
        guard let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ArtistPersonalViewController") as? ArtistPersonalViewController else { return }
        
        presentAnimate()
        
        let title = artistName.currentTitle!
        viewController.navigationItem.title = title
        viewController.navigationController?.navigationBar.tintColor = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        show(viewController, sender: self)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()


        self.preViewImage.image = image!
        setRightBarButtonItem()
        // Do any additional setup after loading the view.
    }
    
    func setRightBarButtonItem() {
        
        let shoppingKartImage = UIImage(named: "ShoppingKart")
        let chattingImage = UIImage(named: "message")
        let searchImage = UIImage(named: "Search")
        
        let shoppingButton = UIButton(type: .custom)
        shoppingButton.setImage(shoppingKartImage, for: UIControlState.normal)
        shoppingButton.addTarget(self, action: #selector(didTapShoppingButton(sender:)), for: UIControlEvents.touchUpInside)
        shoppingButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let shoppingBarButton = UIBarButtonItem(customView: shoppingButton)
        
        let chattingButton = UIButton(type: .custom)
        chattingButton.setImage(chattingImage, for: UIControlState.normal)
        chattingButton.addTarget(self, action: #selector(didTapChattingButton(sender:)), for: UIControlEvents.touchUpInside)
        chattingButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let chattingBarButton = UIBarButtonItem(customView: chattingButton)
        
        let searchingButton = UIButton(type: .custom)
        searchingButton.setImage(searchImage, for: UIControlState.normal)
        searchingButton.addTarget(self, action: #selector(didTapSearchButton(sender:)), for: UIControlEvents.touchUpInside)
        searchingButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let searchingBarButton = UIBarButtonItem(customView: searchingButton)
        
        navigationItem.rightBarButtonItems = [chattingBarButton, shoppingBarButton, searchingBarButton]
        navigationController?.navigationBar.tintColor = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        
    }
    
    func setCardImage(image: UIImage) {
        self.image = image
    }

}

extension CardPreviewViewController: ZoomingViewController {
    func zoomingImageView(for transition: ZoomTransitioningDelegate) -> UIImageView? {
        return preViewImage
    }
    
    func zoomingBackgroundView(for transition: ZoomTransitioningDelegate) -> UIView? {
        return nil
    }
    
}
