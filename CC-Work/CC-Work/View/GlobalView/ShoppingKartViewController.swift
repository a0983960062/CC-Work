//
//  ShoppingKartViewController.swift
//  CC-Work
//
//  Created by Charles Chiang on 2018/2/14.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class ShoppingKartViewController: UIViewController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "購物車"
        self.navigationController?.navigationBar.tintColor = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
